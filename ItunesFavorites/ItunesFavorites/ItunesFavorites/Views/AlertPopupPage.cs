﻿using Rg.Plugins.Popup.Extensions;
using Rg.Plugins.Popup.Pages;
using System;
using Xamarin.Forms;

namespace ItunesFavorites.Views
{
    public class AlertPopupPage : PopupPage
    {
        public AlertPopupPage(string popupMessage)
        {
            var stcContent = new StackLayout
            {
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                Spacing = 12,
                BackgroundColor = Color.White,
                WidthRequest = 200,
                Margin = new Thickness(15, 15),
                Padding = new Thickness(13)
            };

            var lblTitle = new Label
            {
                Text = "Alerta: ",
                FontAttributes = FontAttributes.Bold,
                FontSize = 20,
                HorizontalOptions = LayoutOptions.Center,
                HorizontalTextAlignment = TextAlignment.Center
            };

            var lblDescription = new Label
            {
                Text = popupMessage,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                FontSize = 13,
                HorizontalTextAlignment = TextAlignment.Center
            };

            var btnOk = new Button
            {
                Text = "Ok",
                TextColor = Color.White,
                BackgroundColor = Color.FromHex("f6a03d"),
                BorderRadius = 15,
                HeightRequest = 45,
                WidthRequest = 60,
                MinimumHeightRequest = 45,
                HorizontalOptions = LayoutOptions.Center
            };

            stcContent.Children.Add(lblTitle);
            stcContent.Children.Add(lblDescription);
            stcContent.Children.Add(btnOk);

            Content = stcContent;

            btnOk.Clicked += closePopupPage;
        }

        private void closePopupPage(object sender, EventArgs e)
        {
            Navigation.PopPopupAsync();
        }

    }
}
