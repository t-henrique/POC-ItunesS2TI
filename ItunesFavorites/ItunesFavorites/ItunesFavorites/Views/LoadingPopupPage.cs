﻿using System;
using Rg.Plugins.Popup.Pages;
using Xamarin.Forms;

namespace ItunesFavorites.Views
{
    public class LoadingPopupPage : PopupPage
    {
		public LoadingPopupPage()
		{
			var stcContent = new StackLayout
			{
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				Spacing = 12,
				BackgroundColor = Color.Transparent,
				WidthRequest = 200,
				Margin = new Thickness(15, 15),
				Padding = new Thickness(13)
			};

			var aI = new ActivityIndicator()
			{
				IsRunning = true,
				IsVisible = true,
				Color = Color.White,
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				VerticalOptions = LayoutOptions.CenterAndExpand,
				WidthRequest = 200,
				HeightRequest = 200
			};

			stcContent.Children.Add(aI);

			Content = stcContent;

		}

		protected override bool OnBackgroundClicked()
		{
			return false;
		}
    }
}
