﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ItunesFavorites.ViewModels
{
    public class Music
    {
		public int artistId { get; set; }
		public int trackId { get; set; }
        public string artistName { get; set; }
        public string collectionName { get; set; }
        public string trackName { get; set; }
        public string artworkUrl100 { get; set; }
        
    }
}
