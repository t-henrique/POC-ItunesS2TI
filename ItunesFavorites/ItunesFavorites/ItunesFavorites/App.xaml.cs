﻿using ItunesFavorites.Business;
using ItunesFavorites.Views;
using Plugin.Connectivity;
using Plugin.Connectivity.Abstractions;
using Rg.Plugins.Popup.Extensions;
using Xamarin.Forms;

namespace ItunesFavorites
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            CrossConnectivity.Current.ConnectivityChanged += (sender, e) => verifyInternetStateConnection(sender, e);

            MainPage = new TabbedMainPage();
        }     

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }

        private void verifyInternetStateConnection(object sender, ConnectivityChangedEventArgs e)
        {
            var internetConnectivity = new InternetConnectivity();

            if(!internetConnectivity.isInternetConnected())
            {
                var popupPage = new AlertPopupPage("Sem conexão com a internet!"); 
                MainPage.Navigation.PushPopupAsync(popupPage);
            }
        }
    }
}
