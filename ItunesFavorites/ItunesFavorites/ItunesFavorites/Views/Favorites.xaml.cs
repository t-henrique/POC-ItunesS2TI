﻿using ItunesFavorites.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ItunesFavorites.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Favorites : ContentPage
    {
        private static ObservableCollection<Music> favorites = new ObservableCollection<Music>();

        public Favorites()
        {
            Title = "Favoritas";

            InitializeComponent();

            var favorit= new ObservableCollection<Music>();

            favorit.Add(new Music { artistId = 0, artistName = "", collectionName = "", trackName = "", artworkUrl100 = "" });

			lstView.ItemsSource = favorit;
        }

        public Favorites(Music music)
        {
            addMusicToFavoriteList(music);
        }

        public void addMusicToFavoriteList(Music music)
        {
            favorites.Add(music);
        }

        private void fillFavoriteList(){
            lstView.ItemsSource = favorites;
        }

        protected override void OnAppearing()
        {
            fillFavoriteList();
        }
    }
}
