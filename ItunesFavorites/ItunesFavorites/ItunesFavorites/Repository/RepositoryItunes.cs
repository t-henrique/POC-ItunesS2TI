﻿using ItunesFavorites.Business;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace ItunesFavorites.Repository
{
    public class RepositoryItunes
    {
        private static HttpClient clientHttpDefault()
        {
            HttpClient client = new HttpClient()
            {
                MaxResponseContentBufferSize = 256000
            };
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.Timeout = new TimeSpan(0, 0, 30);

            return client;
        }

        public async Task<ReturnItunes> getItunesMusic(string textSearch)
        {
            var itunesCollection = new ReturnItunes();

            try
            {
                var client = clientHttpDefault();
                var uri = String.Format("https://itunes.apple.com/search?entity=song&limit=30&term='{0}'", textSearch);

                var response = await client.GetAsync(uri);

                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();

                    itunesCollection = JsonConvert.DeserializeObject<ReturnItunes>(content);
                }
                else
                {
                    itunesCollection.resultCount = 0;
                    itunesCollection.errorResult = new ErrorResult()
                    {
                        errorMessage = response.ReasonPhrase,
                        statusCode = (int)response.StatusCode
                    };
                }
            }

            catch (Exception ex)
            {
                itunesCollection.resultCount = 0;
                itunesCollection.errorResult = new ErrorResult()
                {
                    errorMessage = String.Format("Houve um erro na requisição: '{0}'", ex.Message),
                    statusCode = (int)HttpStatusCode.BadRequest
                };
            }

            return itunesCollection;

        }
          
    }
}
