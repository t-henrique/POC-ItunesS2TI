﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using ItunesFavorites.ViewModels;
using Xamarin.Forms;
using ItunesFavorites.Repository;
using System.Threading.Tasks;
using System.Windows.Input;
using System;
using System.Linq;
using ItunesFavorites.Business;
using Rg.Plugins.Popup.Extensions;

namespace ItunesFavorites.Views
{
    public partial class MusicPage : ContentPage
    {
        public ObservableCollection<Music> musics { get; set; }
        public MusicPage()
        {
            InitializeComponent();

            Title = "Pesquisar";

            musics = new ObservableCollection<Music>();
            lstView.ItemsSource = musics;

            searchMusic.SearchButtonPressed += (sender, e) => doSearchOnRepository(searchMusic.Text);

        }

        private async Task doSearchOnRepository(string textSearch)
        {
            await Navigation.PushPopupAsync(new LoadingPopupPage());

            var musics = new List<Music>();
            var requestDataRepository = new RepositoryItunes();

            var returnData = await searchData(textSearch);

            await Navigation.PopPopupAsync();

            if (returnData.errorResult == null )
            {
                await fillMusicsItemsSoure(returnData.results);
          	}
            else
            {
                await Navigation.PushPopupAsync(new AlertPopupPage(returnData.errorResult.errorMessage));
            }

        }

        private async Task<ReturnItunes> searchData(string textSearch)
        {
            var requestDataRepository = new RepositoryItunes();

            var itunesCollection = new ReturnItunes();

            if (checkInternetConnection())
            {
                itunesCollection = await requestDataRepository.getItunesMusic(textSearch);
            }
            else
            {
                itunesCollection.resultCount = 0;
                itunesCollection.errorResult = new ErrorResult()
                {
                    errorMessage = "Sem conexão com internet.",
                    statusCode = 503

                };
            }
            return itunesCollection;
        }

		private async Task fillMusicsItemsSoure(List<Music> results)
		{
			musics.Clear();
			foreach (Music music in results)
			{
				musics.Add(music);
			}
		}

		private bool checkInternetConnection()
        {
            var internetConnectivity = new InternetConnectivity();

            return internetConnectivity.isInternetConnected();
        }

        void removeFromList(object sender, System.EventArgs e)
        {
            var item = (Xamarin.Forms.Button)sender;
            Music listitem = (from itm in musics
                              where itm.trackId == int.Parse(item.CommandParameter.ToString())
                              select itm).FirstOrDefault<Music>();
            musics.Remove(listitem);
        }

		void addOnFavoriteList(object sender, System.EventArgs e)
		{
            var item = (Xamarin.Forms.Button)sender;
            
            Music listitem = (from itm in musics
                              where itm.trackId == int.Parse(item.CommandParameter.ToString())
                              select itm).FirstOrDefault<Music>();

			var favoriteList = new Favorites(listitem);

			musics.Remove(listitem);
		}

    }
}

