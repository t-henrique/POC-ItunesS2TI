﻿using ItunesFavorites.ViewModels;
using System.Collections.Generic;

namespace ItunesFavorites.Repository
{
    public class ReturnItunes
    {
        public int resultCount { get; set; }
        public List<Music> results { get; set; }
        public ErrorResult errorResult { get; set; }
    }
}