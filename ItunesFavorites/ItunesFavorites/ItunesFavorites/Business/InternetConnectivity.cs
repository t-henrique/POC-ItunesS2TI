﻿using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ItunesFavorites.Business
{
    public class InternetConnectivity
    {
        bool isConnected { get; }

        public bool isInternetConnected()
        {
            if (!CrossConnectivity.IsSupported)
                return true;

            var connectivity = CrossConnectivity.Current;

            return connectivity.IsConnected;


        }
    }
        
}
