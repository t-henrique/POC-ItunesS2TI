﻿namespace ItunesFavorites.Repository
{
    public class ErrorResult
    {
        public string errorMessage { get; set; } = null;
        public int? statusCode { get; set; }
    }
}